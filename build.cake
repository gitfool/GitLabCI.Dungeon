#load nuget:?package=Cake.Dungeon&prerelease

Build.SetParameters
(
    title: "GitLabCI.Dungeon",

    logEnvironment: true,
    logBuildSystem: true
);

Build.Run();
