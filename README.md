# GitLabCI.Dungeon

[![License](https://img.shields.io/badge/license-MIT-blue.svg?label=License&logo=github)](LICENSE)
[![GitLab CI](https://img.shields.io/gitlab/pipeline/gitfool/GitLabCI.Dungeon/main?label=GitLab%20CI&logo=gitlab)](https://gitlab.com/gitfool/GitLabCI.Dungeon/pipelines)

`GitLabCI.Dungeon` is a playground for [GitLab CI](https://docs.gitlab.com/ee/ci).
